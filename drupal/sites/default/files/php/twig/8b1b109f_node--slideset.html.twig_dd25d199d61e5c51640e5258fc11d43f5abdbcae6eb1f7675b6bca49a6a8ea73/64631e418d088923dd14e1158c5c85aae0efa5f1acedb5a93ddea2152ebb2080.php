<?php

/* themes/reveal/templates/node-types/node--slideset.html.twig */
class __TwigTemplate_df847b5e24df2143b84b34f131091fd59fe7c1d06a7437a2c6c6fb1d18791f76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'node' => array($this, 'block_node'),
            'node_content' => array($this, 'block_node_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("block" => 1);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('block'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        $this->displayBlock('node', $context, $blocks);
    }

    public function block_node($context, array $blocks = array())
    {
        // line 2
        echo "    ";
        $this->displayBlock('node_content', $context, $blocks);
    }

    public function block_node_content($context, array $blocks = array())
    {
        // line 3
        echo "      ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["content"]) ? $context["content"] : null), "html", null, true));
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "themes/reveal/templates/node-types/node--slideset.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  58 => 3,  51 => 2,  45 => 1,);
    }
}
/* {% block node %}*/
/*     {% block node_content %}*/
/*       {{ content }}*/
/*     {% endblock node_content %}*/
/* {% endblock node %}*/
/* */
