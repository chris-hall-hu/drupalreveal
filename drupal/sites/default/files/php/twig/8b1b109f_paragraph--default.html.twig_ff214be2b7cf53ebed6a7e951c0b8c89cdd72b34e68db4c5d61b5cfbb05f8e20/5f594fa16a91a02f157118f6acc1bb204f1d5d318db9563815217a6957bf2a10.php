<?php

/* themes/reveal/templates/paragraphs/paragraph--default.html.twig */
class __TwigTemplate_43f7534f579c00e5d8a99d9d3a4807e75ff56f775e4cd060d1897c7f56eb4d6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 8, "if" => 15);
        $filters = array("clean_class" => 10, "without" => 27);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set', 'if'),
                array('clean_class', 'without'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 8
        $context["classes"] = array(0 => "paragraph", 1 => ("paragraph--type--" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(        // line 10
(isset($context["paragraph"]) ? $context["paragraph"] : null), "bundle", array()))), 2 => ((        // line 11
(isset($context["view_mode"]) ? $context["view_mode"] : null)) ? (("paragraph--view-mode--" . \Drupal\Component\Utility\Html::getClass((isset($context["view_mode"]) ? $context["view_mode"] : null)))) : ("")));
        // line 14
        echo "
";
        // line 15
        if ( !twig_test_empty($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_slides", array()))) {
            // line 16
            echo "  ";
            $context["is_parent"] = true;
        } else {
            // line 18
            echo "  ";
            $context["is_parent"] = false;
        }
        // line 20
        echo "<section ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["classes"]) ? $context["classes"] : null)), "method"), "html", null, true));
        echo ">
    ";
        // line 21
        if ((isset($context["is_parent"]) ? $context["is_parent"] : null)) {
            echo "<section>";
        }
        // line 22
        echo "    ";
        if ( !twig_test_empty($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_slide_title", array()))) {
            // line 23
            echo "    <h1>
      ";
            // line 24
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_slide_title", array()), "html", null, true));
            echo "
    </h1>
    ";
        }
        // line 27
        echo "    ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, twig_without((isset($context["content"]) ? $context["content"] : null), "field_slide_title", "field_slides"), "html", null, true));
        echo "
    ";
        // line 28
        if ((isset($context["is_parent"]) ? $context["is_parent"] : null)) {
            echo "</section>";
        }
        // line 29
        echo "  ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_slides", array()), "html", null, true));
        echo "
</section>
";
    }

    public function getTemplateName()
    {
        return "themes/reveal/templates/paragraphs/paragraph--default.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 29,  86 => 28,  81 => 27,  75 => 24,  72 => 23,  69 => 22,  65 => 21,  60 => 20,  56 => 18,  52 => 16,  50 => 15,  47 => 14,  45 => 11,  44 => 10,  43 => 8,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Default reveal implementation to display a paragraph.*/
/*  *//* */
/* #}*/
/* {%*/
/* set classes = [*/
/* 'paragraph',*/
/* 'paragraph--type--' ~ paragraph.bundle|clean_class,*/
/* view_mode ? 'paragraph--view-mode--' ~ view_mode|clean_class,*/
/* ]*/
/* %}*/
/* */
/* {% if content.field_slides is not empty %}*/
/*   {% set is_parent = true %}*/
/* {% else %}*/
/*   {% set is_parent = false %}*/
/* {% endif %}*/
/* <section {{ attributes.addClass(classes) }}>*/
/*     {% if is_parent %}<section>{% endif %}*/
/*     {% if content.field_slide_title is not empty %}*/
/*     <h1>*/
/*       {{ content.field_slide_title }}*/
/*     </h1>*/
/*     {% endif %}*/
/*     {{ content|without('field_slide_title', 'field_slides') }}*/
/*     {% if is_parent %}</section>{% endif %}*/
/*   {{ content.field_slides }}*/
/* </section>*/
/* */
