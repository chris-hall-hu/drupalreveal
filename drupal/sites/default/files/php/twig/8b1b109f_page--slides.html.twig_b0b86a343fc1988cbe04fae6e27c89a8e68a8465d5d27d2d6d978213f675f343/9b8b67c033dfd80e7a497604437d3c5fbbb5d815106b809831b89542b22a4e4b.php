<?php

/* page--slides.html.twig */
class __TwigTemplate_b68db6d274db823827a00264981079051eb02af2b309ff94f5a2c59619861ce7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'page_main' => array($this, 'block_page_main'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("block" => 3);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('block'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "
<div class=\"reveal\">
  ";
        // line 3
        $this->displayBlock('page_main', $context, $blocks);
        // line 8
        echo "</div>

";
    }

    // line 3
    public function block_page_main($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"slides\">
        ";
        // line 5
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
        echo "
    </div>
  ";
    }

    public function getTemplateName()
    {
        return "page--slides.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  62 => 5,  59 => 4,  56 => 3,  50 => 8,  48 => 3,  44 => 1,);
    }
}
/* */
/* <div class="reveal">*/
/*   {% block page_main %}*/
/*     <div class="slides">*/
/*         {{ page.content }}*/
/*     </div>*/
/*   {% endblock page_main %}*/
/* </div>*/
/* */
/* */
