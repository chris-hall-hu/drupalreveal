<?php

/* themes/reveal/templates/html/html.html.twig */
class __TwigTemplate_2f9fc0378c62438460c9f4087cbae0ffdb1517aa3d3a6f62fae935b5aa080ac9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'html_head' => array($this, 'block_html_head'),
            'html_title' => array($this, 'block_html_title'),
            'html_styles' => array($this, 'block_html_styles'),
            'html_js' => array($this, 'block_html_js'),
            'html_body' => array($this, 'block_html_body'),
            'html_skip_content' => array($this, 'block_html_skip_content'),
            'html_page_top' => array($this, 'block_html_page_top'),
            'html_page' => array($this, 'block_html_page'),
            'html_page_bottom' => array($this, 'block_html_page_bottom'),
            'html_js_bottom' => array($this, 'block_html_js_bottom'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("block" => 4);
        $filters = array("raw" => 6, "safe_join" => 8, "t" => 22);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('block'),
                array('raw', 'safe_join', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "
<!DOCTYPE html>
<html";
        // line 3
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["html_attributes"]) ? $context["html_attributes"] : null), "html", null, true));
        echo ">
  ";
        // line 4
        $this->displayBlock('html_head', $context, $blocks);
        // line 18
        echo "  ";
        $this->displayBlock('html_body', $context, $blocks);
        // line 63
        echo "</html>
";
    }

    // line 4
    public function block_html_head($context, array $blocks = array())
    {
        // line 5
        echo "    <head>
      <head-placeholder token=\"";
        // line 6
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["placeholder_token"]) ? $context["placeholder_token"] : null)));
        echo "\">
      ";
        // line 7
        $this->displayBlock('html_title', $context, $blocks);
        // line 10
        echo "      ";
        $this->displayBlock('html_styles', $context, $blocks);
        // line 13
        echo "      ";
        $this->displayBlock('html_js', $context, $blocks);
        // line 16
        echo "    </head>
  ";
    }

    // line 7
    public function block_html_title($context, array $blocks = array())
    {
        // line 8
        echo "        <title>";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->safeJoin($this->env, (isset($context["head_title"]) ? $context["head_title"] : null), " | ")));
        echo "</title>
      ";
    }

    // line 10
    public function block_html_styles($context, array $blocks = array())
    {
        // line 11
        echo "        <css-placeholder token=\"";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["placeholder_token"]) ? $context["placeholder_token"] : null)));
        echo "\">
      ";
    }

    // line 13
    public function block_html_js($context, array $blocks = array())
    {
        // line 14
        echo "        <js-placeholder token=\"";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["placeholder_token"]) ? $context["placeholder_token"] : null)));
        echo "\">
      ";
    }

    // line 18
    public function block_html_body($context, array $blocks = array())
    {
        // line 19
        echo "    <body";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["attributes"]) ? $context["attributes"] : null), "html", null, true));
        echo ">
      ";
        // line 20
        $this->displayBlock('html_skip_content', $context, $blocks);
        // line 25
        echo "      ";
        $this->displayBlock('html_page_top', $context, $blocks);
        // line 28
        echo "      ";
        $this->displayBlock('html_page', $context, $blocks);
        // line 31
        echo "      ";
        $this->displayBlock('html_page_bottom', $context, $blocks);
        // line 34
        echo "      ";
        $this->displayBlock('html_js_bottom', $context, $blocks);
        // line 37
        echo "      <script>

\t\t\t// Full list of configuration options available at:
\t\t\t// https://github.com/hakimel/reveal.js#configuration
\t\t\tReveal.initialize({
\t\t\t\tcontrols: true,
\t\t\t\tprogress: true,
\t\t\t\thistory: true,
\t\t\t\tcenter: true,

\t\t\t\ttransition: 'slide', // none/fade/slide/convex/concave/zoom

\t\t\t\t// Optional reveal.js plugins
\t\t\t\tdependencies: [
\t\t\t\t\t{ src: 'lib/js/classList.js', condition: function() { return !document.body.classList; } },
\t\t\t\t\t{ src: 'plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
\t\t\t\t\t{ src: 'plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
\t\t\t\t\t{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
\t\t\t\t\t{ src: 'plugin/zoom-js/zoom.js', async: true },
\t\t\t\t\t{ src: 'plugin/notes/notes.js', async: true }
\t\t\t\t]
\t\t\t});

\t\t  </script>
    </body>
  ";
    }

    // line 20
    public function block_html_skip_content($context, array $blocks = array())
    {
        // line 21
        echo "        <a href=\"#main-content\" class=\"visually-hidden focusable\">
          ";
        // line 22
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Skip to main content")));
        echo "
        </a>
      ";
    }

    // line 25
    public function block_html_page_top($context, array $blocks = array())
    {
        // line 26
        echo "        ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["page_top"]) ? $context["page_top"] : null), "html", null, true));
        echo "
      ";
    }

    // line 28
    public function block_html_page($context, array $blocks = array())
    {
        // line 29
        echo "        ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["page"]) ? $context["page"] : null), "html", null, true));
        echo "
      ";
    }

    // line 31
    public function block_html_page_bottom($context, array $blocks = array())
    {
        // line 32
        echo "        ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["page_bottom"]) ? $context["page_bottom"] : null), "html", null, true));
        echo "
      ";
    }

    // line 34
    public function block_html_js_bottom($context, array $blocks = array())
    {
        // line 35
        echo "        <js-bottom-placeholder token=\"";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["placeholder_token"]) ? $context["placeholder_token"] : null)));
        echo "\">
      ";
    }

    public function getTemplateName()
    {
        return "themes/reveal/templates/html/html.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 35,  218 => 34,  211 => 32,  208 => 31,  201 => 29,  198 => 28,  191 => 26,  188 => 25,  181 => 22,  178 => 21,  175 => 20,  146 => 37,  143 => 34,  140 => 31,  137 => 28,  134 => 25,  132 => 20,  127 => 19,  124 => 18,  117 => 14,  114 => 13,  107 => 11,  104 => 10,  97 => 8,  94 => 7,  89 => 16,  86 => 13,  83 => 10,  81 => 7,  77 => 6,  74 => 5,  71 => 4,  66 => 63,  63 => 18,  61 => 4,  57 => 3,  53 => 1,);
    }
}
/* */
/* <!DOCTYPE html>*/
/* <html{{ html_attributes }}>*/
/*   {% block html_head %}*/
/*     <head>*/
/*       <head-placeholder token="{{ placeholder_token|raw }}">*/
/*       {% block html_title %}*/
/*         <title>{{ head_title|safe_join(' | ') }}</title>*/
/*       {% endblock html_title %}*/
/*       {% block html_styles %}*/
/*         <css-placeholder token="{{ placeholder_token|raw }}">*/
/*       {% endblock html_styles %}*/
/*       {% block html_js %}*/
/*         <js-placeholder token="{{ placeholder_token|raw }}">*/
/*       {% endblock html_js %}*/
/*     </head>*/
/*   {% endblock html_head %}*/
/*   {% block html_body %}*/
/*     <body{{ attributes }}>*/
/*       {% block html_skip_content %}*/
/*         <a href="#main-content" class="visually-hidden focusable">*/
/*           {{ 'Skip to main content'|t }}*/
/*         </a>*/
/*       {% endblock html_skip_content %}*/
/*       {% block html_page_top %}*/
/*         {{ page_top }}*/
/*       {% endblock html_page_top %}*/
/*       {% block html_page %}*/
/*         {{ page }}*/
/*       {% endblock html_page %}*/
/*       {% block html_page_bottom %}*/
/*         {{ page_bottom }}*/
/*       {% endblock html_page_bottom %}*/
/*       {% block html_js_bottom %}*/
/*         <js-bottom-placeholder token="{{ placeholder_token|raw }}">*/
/*       {% endblock html_js_bottom %}*/
/*       <script>*/
/* */
/* 			// Full list of configuration options available at:*/
/* 			// https://github.com/hakimel/reveal.js#configuration*/
/* 			Reveal.initialize({*/
/* 				controls: true,*/
/* 				progress: true,*/
/* 				history: true,*/
/* 				center: true,*/
/* */
/* 				transition: 'slide', // none/fade/slide/convex/concave/zoom*/
/* */
/* 				// Optional reveal.js plugins*/
/* 				dependencies: [*/
/* 					{ src: 'lib/js/classList.js', condition: function() { return !document.body.classList; } },*/
/* 					{ src: 'plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },*/
/* 					{ src: 'plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },*/
/* 					{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },*/
/* 					{ src: 'plugin/zoom-js/zoom.js', async: true },*/
/* 					{ src: 'plugin/notes/notes.js', async: true }*/
/* 				]*/
/* 			});*/
/* */
/* 		  </script>*/
/*     </body>*/
/*   {% endblock html_body %}*/
/* </html>*/
/* */
